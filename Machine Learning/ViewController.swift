//
//  ViewController.swift
//  Machine Learning
//
//  Created by Atakan Cengiz KURT on 29.10.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var chosenImage = CIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func action_takePhoto(_ sender: Any) {
        
        let picker = UIImagePickerController()  //imagepicker tanımlıyoruz
        picker.delegate = self  //delege fonksiyonlarını kullanacağımızı bildiriyoruz
        picker.allowsEditing = true  //düzenlemeye izin veriyoruz
        picker.sourceType = .photoLibrary  //.camera ya da photoLibrary'den görsel seçeceğiz
        self.present(picker, animated: true, completion: nil)  //
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        image.image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        self.dismiss(animated: true, completion: nil)
        
        if let ciImage = CIImage(image: image.image!){
            self.chosenImage = ciImage
        }
       recognizeImage(image: chosenImage)
        
    }
    
    func recognizeImage(image: CIImage){
        label.text = "Finding..."
        
        if let model = try? VNCoreMLModel(for: GoogLeNetPlaces().model){
            
            let request = VNCoreMLRequest(model: model) { (vnrequest, error) in
                if let results = vnrequest.results as? [VNClassificationObservation]{
                    let topResult = results.first!
                    print("results=\(results)")
                    DispatchQueue.main.async {
                        let conf = (topResult.confidence) * 100
                        
                        let rounded = Int(conf)
                        
                        self.label.text = "\(rounded)% it's \(topResult.identifier) "
                    }
                }
            }
            let handler = VNImageRequestHandler(ciImage: image)
            DispatchQueue.global(qos: .userInteractive).async {
                do{
                    try handler.perform([request])
                    
                }catch{
                    print("error")
                }
            }
        }
    }
    
    //MARK: -CoreML
    
    
}

